<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/listUsers", name="listUsers")
     */
    public function index(): Response
    {

        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('registration/users/listusers.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/addUser", name="addUser")
     */
    public function addUser(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        //Roles
        $roles = $this->getParameter('security.role_hierarchy.roles');

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            $this->addFlash('user_add','New User Created Successfully');

            return $this->redirectToRoute('addUser');
        }

        return $this->render('registration/users/adduser.html.twig',[
            'registrationForm' => $form->createView(),
            'roles' => $roles
        ]);
    }

    /**
     * @Route("/editUser/{id}", name="editUser")
     */
    public function edit(Request $request, $id, UserPasswordEncoderInterface $passwordEncoder)
    {
        //Roles
        $roles = $this->getParameter('security.role_hierarchy.roles');

        //Users
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            $this->addFlash('user_update','Updated Successfully');

            return $this->redirectToRoute('listUsers');
        }

        return $this->render('registration/users/edituser.html.twig',[
            'registrationForm' => $form->createView(),
            'roles' => $roles,
        ]);
    }

    /**
     * @Route("/deleteUser/{id}", name="deleteUser")
     */
    public function delete($id)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('user_delete','Deleted Successfully');

        return $this->redirectToRoute('listUsers');
    }
}
